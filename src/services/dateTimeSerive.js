import moment from 'moment';

export const formatLastMessageDateTime = (dateTime) => {
  const dt = moment(dateTime);
  const now = moment();
  let format;
  if (dt.year() === now.year() && dt.month() === now.month() && dt.date() === now.date()) {
    format = 'H:mm';
  } else if (now.date() - dt.date() <= 7) {
    format = 'dddd';
  } else {
    format = 'D.MM.YY';
  }
  return dt.format(format);
};

export const formatMessagesDivisorDate = (dateTime) => {
  const dt = moment(dateTime);
  const now = moment();
  const format = (dt.year() === now.year()) ? 'Do MMMM' : 'D.MM.YYYY';
  return dt.format(format);
};
