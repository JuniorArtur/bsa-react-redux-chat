import React from 'react';
import { render } from 'react-dom';
import Chat from './containers/Chat';
import { Provider } from 'react-redux';
import store from './store';

import 'semantic-ui-css/semantic.min.css';
import './styles/common.css';

render(
  <React.StrictMode>
    <Provider store={store}>
      <Chat/>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
