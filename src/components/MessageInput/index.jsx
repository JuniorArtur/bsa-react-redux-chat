import React, { useState } from 'react';
import { Button, Icon, TextArea } from 'semantic-ui-react';
import './styles.css';

export const MessageInput = ({ sendMessage }) => {
  const [messageBody, setMessageBody] = useState();
  return (
    <div className='field message-input'>
      <TextArea
        placeholder='Message'
        value={messageBody}
        className='message-input__field'
        onChange={e => setMessageBody(e.target.value)}
      />
      <Button
        className='message-input__btn'
        size='huge'
        labelPosition='right'
        onClick={() => {
          if (messageBody.trim()) {
            sendMessage(messageBody);
            setMessageBody('');
          }
        }}
      >
        <Icon name='location arrow'/> Send
      </Button>
    </div>
  );
};

export default MessageInput;
