import React from 'react';
import Message from '../Message';
import { formatMessagesDivisorDate } from '../../services/dateTimeSerive';

import './styles.css';
import '../Message/styles.css';

export const MessageList = ({ currentUser, messagesByDatesMap, likeMessage, editMessage, deleteMessage }) => {
  const renderMessagesDivider = date =>
    <div className='message-divider'>
      <span className='message-divider__line'/>
      <p className='message-divider__date'>{formatMessagesDivisorDate(date)}</p>
      <span className='message-divider__line'/>
    </div>;

  const renderMessages = messages =>
    messages.map(message =>
      <ul className='message-list' key={message.id}>
        <li className={'message message--' + (message.user === currentUser ? 'out' : 'in')}>
          <Message
            message={message}
            author={currentUser}
            likeMessage={likeMessage}
            editMessage={editMessage}
            deleteMessage={deleteMessage}
          />
        </li>
      </ul>
    );

  return (
    <>
      {
        messagesByDatesMap.map((dateMessages, index) =>
          <div key={{index}}>
            {renderMessagesDivider(dateMessages.date)}
            {renderMessages(dateMessages.messages)}
          </div>
        )
      }
    </>
  );
};

export default MessageList;
