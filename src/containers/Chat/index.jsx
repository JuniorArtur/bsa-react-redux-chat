import React from 'react';
import { Menu } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import uuid from 'uuid';
import moment from 'moment';
import Spinner from '../../components/Spinner';
import Logo from '../../components/Logo';
import Header from '../../components/Header';
import MessageList from '../../components/MessageList';
import MessageInput from '../../components/MessageInput';
import { addMessage, deleteMessage, editMessage, likeMessage, loadChatInfo, loadMessages } from './actions';
import { getMessagesSortedByDate, sortMessagesPerDates } from '../../services/messageService';

import logoImage from './chat-logo.svg';
import './styles.css';

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      chatOwner: {
        id: uuid(),
        name: 'Artur'
      }
    };
  }

  async componentDidMount() {
    const sortedByDateMessages = await getMessagesSortedByDate(
      'https://edikdolynskyi.github.io/react_sources/messages.json'
    );
    const usersCount = [...new Set(sortedByDateMessages.map(m => m.user))].length + 1;
    this.props.loadMessages(sortedByDateMessages);
    this.props.loadChatInfo({
      chatName: 'Devs chat',
      chatOwner: this.state.chatOwner,
      usersCount: usersCount,
      messagesCount: sortedByDateMessages.length,
      lastMessageDate: sortedByDateMessages[sortedByDateMessages.length - 1].createdAt
    });
    this.setState({ isLoading: false });
  }

  render() {
    const { chatOwner, isLoading } = this.state;
    const { messagesCount, usersCount, chatName, lastMessageDate } = this.props.chatInfo;
    const logoTitle = 'Chatify';
    const strDates = this.props.messages.map(m => m.createdAt.substring(0, 10));
    const uniqueSortedDates = [...new Set(strDates)].map(d => moment(d));
    const messagesPerDates = sortMessagesPerDates(this.props.messages, uniqueSortedDates);

    return (
      isLoading
        ? <Spinner/>
        : (
          <div className='chat'>
            <Logo image={logoImage} title={logoTitle}/>
            <Menu className='chat__header' borderless>
              <Menu.Item as='h1' header>
                <Header
                  chatName={chatName}
                  usersCount={usersCount}
                  messagesCount={messagesCount}
                  lastMessageDate={lastMessageDate}
                />
              </Menu.Item>
            </Menu>
            <main className='chat__content'>
              <MessageList
                currentUser={chatOwner.name}
                messages={this.props.messages}
                messagesByDatesMap={messagesPerDates}
                likeMessage={this.props.likeMessage}
                editMessage={this.props.editMessage}
                deleteMessage={this.props.deleteMessage}
              />
            </main>
            <Menu className='chat__footer' borderless>
              <Menu.Item header>
                <MessageInput sendMessage={this.props.addMessage}/>
              </Menu.Item>
            </Menu>
            <footer className='chat__footer-title'>
              <p className='chat-footer__text'>&copy; BSA Team</p>
            </footer>
          </div>
        )
    );
  }
}

const mapStateToProps = rootState => ({
  messages: rootState.chat.messages,
  chatInfo: rootState.chat.chatInfo
});

const actions = {
  loadMessages,
  loadChatInfo,
  likeMessage,
  addMessage,
  editMessage,
  deleteMessage
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Chat);
