import React from 'react';

import './styles.css';

export const Logo = ({ image, title }) => (
  <div className='app__logo-container'>
    <div className='app__logo'>
      <img src={image} className='app__logo-img' alt='logo'/>
    </div>
    <h2 className='app__title'>{title}</h2>
  </div>
);

export default Logo;
