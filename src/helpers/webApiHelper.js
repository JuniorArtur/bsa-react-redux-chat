export async function throwIfResponseFailed(res) {
  if (!res.ok) {
    let parsedException = 'Something went wrong with request!';
    try {
      parsedException = await res.json();
    } catch (err) {
      console.error(err);
    }
    throw parsedException;
  }
}

export default async function sendGetRequest(url) {
  const res = await fetch(url);
  await throwIfResponseFailed(res);
  return res;
}
