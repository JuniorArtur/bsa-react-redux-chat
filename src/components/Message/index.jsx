import React from 'react';
import { Button, Dropdown, Feed, Form, Icon, Modal, TextArea } from 'semantic-ui-react';
import moment from 'moment';

import './styles.css';

export default class Message extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalOpen: false,
      messageBody: this.props.message.text,
      isLiked: false,
      isHovering: false
    };
    this.handleMouseHover = this.handleMouseHover.bind(this);
  }

  handleMouseHover() {
    this.setState(this.toggleHoverState);
  }

  toggleHoverState(state) {
    return { isHovering: !state.isHovering };
  }

  handleEditing() {
    this.props.editMessage(this.props.message.id, this.state.messageBody);
    this.setState({
      isModalOpen: false,
      isHovering: !this.state.isHovering
    });
  }

  handleDeletion() {
    this.props.deleteMessage(this.props.message.id);
  }

  render() {
    const { isModalOpen, isLiked, isHovering } = this.state;
    const { user, avatar, createdAt, text } = this.props.message;
    const date = moment(createdAt).fromNow();
    const renderMessageOptionsDropdown = (
      <Dropdown inline icon='cog' className='ui right floated message__options'>
        <Dropdown.Menu>
          <Dropdown.Item onClick={() => this.setState({ isModalOpen: true })}>
            <Icon name='edit'/> Edit
          </Dropdown.Item>
          <Dropdown.Item onClick={() => this.handleDeletion()}>
            <Icon name='trash'/> Delete
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    );
    const renderMessageEditingModal = (
      <Modal
        closeIcon
        dimmer='blurring'
        centered={false}
        open={isModalOpen}
        onClose={() => this.setState({
          isModalOpen: false,
          isHovering: !isHovering
        })}
      >
        <Modal.Header>Edit message</Modal.Header>
        <Modal.Content>
          <Form onSubmit={(e) => this.handleEditing(e)}>
            <Form.Group inline>
              <Form.Field
                control={TextArea}
                placeholder='Message'
                onChange={e => this.setState({ messageBody: e.target.value })}
                value={this.state.messageBody}
              />
              <Button color='vk'>Edit</Button>
            </Form.Group>
          </Form>
        </Modal.Content>
      </Modal>
    );
    return (
      <div className='message' onMouseEnter={this.handleMouseHover} onMouseLeave={this.handleMouseHover}>
        {renderMessageEditingModal}
        <Feed>
          <Feed.Event>
            {avatar && <Feed.Label><img src={avatar} alt='avatar'/></Feed.Label>}
            <Feed.Content>
              <Feed.Summary>
                <Feed.User> {user} </Feed.User>
                <Feed.Date> {date} </Feed.Date>
                {(user === this.props.author && isHovering) && renderMessageOptionsDropdown}
              </Feed.Summary>
              <Feed.Extra text> {text} </Feed.Extra>
              <Feed.Meta>
                <Feed.Like onClick={() => this.setState({ isLiked: !isLiked })}>
                  <Icon name='like'/> {(isLiked && user !== this.props.author) ? 1 : 0} likes
                </Feed.Like>
              </Feed.Meta>
            </Feed.Content>
          </Feed.Event>
        </Feed>
      </div>
    );
  }
}
