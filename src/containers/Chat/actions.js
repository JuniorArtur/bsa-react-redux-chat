import { ADD_MESSAGE, DELETE_MESSAGE, EDIT_MESSAGE, LIKE_MESSAGE, LOAD_CHAT_INFO, LOAD_MESSAGES } from './actionTypes';
import uuid from 'uuid';
import moment from 'moment';

const loadMessagesAction = messages => ({
  type: LOAD_MESSAGES,
  messages
});

const loadChatInfoAction = chatInfo => ({
  type: LOAD_CHAT_INFO,
  chatInfo
});

const likeMessageAction = messages => ({
  type: LIKE_MESSAGE,
  messages
});

const addMessageAction = message => ({
  type: ADD_MESSAGE,
  message
});

const editMessageAction = messages => ({
  type: EDIT_MESSAGE,
  messages
});

const deleteMessageAction = messages => ({
  type: DELETE_MESSAGE,
  messages
});

export const loadMessages = messages => async (dispatch) => {
  console.log('loadMessages');
  dispatch(loadMessagesAction(messages));
};

export const loadChatInfo = chatInfo => async (dispatch) => {
  console.log('loadChatInfo');
  dispatch(loadChatInfoAction(chatInfo));
};

export const likeMessage = id => (dispatch, getRootState) => {
  const { chat: { messages } } = getRootState();
  const updatedMessages = messages.map(msg => {
    if (msg.id === id) {
      msg.isLiked = !msg.isLiked;
    }
    return msg;
  });
  dispatch(likeMessageAction(updatedMessages));
};

export const addMessage = (text) => (dispatch, getRootState) => {
  const { chat: { messages, chatInfo } } = getRootState();
  const newMessage = {
    id: uuid(),
    userId: chatInfo.chatOwner.id,
    user: chatInfo.chatOwner.name,
    avatar: null,
    text,
    createdAt: moment()
      .toISOString(true),
    editedAt: null
  };
  dispatch(addMessageAction(newMessage));
  dispatch(loadChatInfoAction({
    ...chatInfo,
    messagesCount: messages.length + 1,
    lastMessageDate: newMessage.createdAt
  }));
};

export const editMessage = (id, text) => (dispatch, getRootState) => {
  const { chat: { messages } } = getRootState();
  const editedMessages = messages.map(msg => {
    if (msg.id === id) {
      msg.text = text;
      msg.editedAt = moment()
        .toISOString(true);
    }
    return msg;
  });
  dispatch(editMessageAction(editedMessages));
};

export const deleteMessage = (id) => (dispatch, getRootState) => {
  const { chat: { messages } } = getRootState();
  const messagesRest = messages.filter(msg => msg.id !== id);
  dispatch(deleteMessageAction(messagesRest));
};
