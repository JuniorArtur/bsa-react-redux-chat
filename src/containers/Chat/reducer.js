import { ADD_MESSAGE, DELETE_MESSAGE, EDIT_MESSAGE, LIKE_MESSAGE, LOAD_CHAT_INFO, LOAD_MESSAGES } from './actionTypes';

const initialState = {
  messages: [],
  chatInfo: {}
};

export default (state = initialState, action) => {
  console.log('state', state);
  console.log('action', action);
  switch (action.type) {
    case LOAD_MESSAGES:
    case LIKE_MESSAGE:
    case EDIT_MESSAGE:
    case DELETE_MESSAGE:
      return {
        ...state,
        messages: action.messages
      };
    case ADD_MESSAGE:
      return {
        ...state,
        messages: [...state.messages, action.message]
      };
    case LOAD_CHAT_INFO:
      return {
        ...state,
        chatInfo: { ...state.chatInfo, ...action.chatInfo }
      };
    default:
      return state;
  }
};
